package in.kahub.org.xbib.io.compress.czlib;

class TypeHelper {

  static int[] toIntBytes(byte[] bytes) {
    int[] arrs = new int[bytes.length];
    for (int i=0; i<bytes.length; i++) {
      arrs[i] = bytes[i] & 0xff;
    }
    return arrs;
  }

}
